<?php namespace Techrino\Instagram\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use October\Rain\Network\Http;
use October\Rain\Support\Facades\Flash;
use Techrino\Instagram\Models\Settings as SettingsModel;
use Backend;
/**
 * Settings Back-end Controller
 */
class Settings extends Controller
{
    public $implement = [
//        'Backend.Behaviors.FormController',
//        'Backend.Behaviors.ListController'
    ];

//    public $formConfig = 'config_form.yaml';
//    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Techrino.Instagram', 'instagram', 'settings');
    }

    public function authenticate(){
//        techrino/instagram/settings/authenticate
        if(Input::has("action")){
            if(Input::get("action") == "logout"){

                SettingsModel::set([
                    "username" => null,
                    "full_name" => null,
                    "user_id" => null,
                    "profile_picture" => null,
                    "access_token" => null
                ]);
                Flash::success("Logged Out Successfully.");
                return redirect()->back();
            }
        }
        $clientID = SettingsModel::get('client_id');
        $clientSecret = SettingsModel::get('client_secret');
            $redirectURL = Backend::url("techrino/instagram/settings/callback");
            if(is_null($clientID) || is_null($clientSecret)){
                Flash::error("Unable to perform oAuth login request");

                return redirect()->back();
            }
            return redirect()->to("https://api.instagram.com/oauth/authorize?client_id=$clientID&redirect_uri=$redirectURL&response_type=code"); // redirect to instagram oauth
    }

    public function callback(){
        if(Input::has("code")){
            $authorizationCode = Input::get("code");
            $redirectURL = Backend::url("techrino/instagram/settings/callback");

             $response = Http::post("https://api.instagram.com/oauth/access_token", function($request)use($redirectURL, $authorizationCode){
                $request->data([
                    "client_id" => SettingsModel::get('client_id'),
                    "client_secret" => SettingsModel::get('client_secret'),
                    "grant_type" => "authorization_code",
                    "redirect_uri" => $redirectURL,
                    "code" => $authorizationCode
                ]);
            });
             $response = json_decode($response, true);
             if(array_key_exists('code' , $response) && $response['code'] == 400){
                 Flash::error($response['error_message']);
                 return redirect()->to(Backend::url("system/settings/update/techrino/instagram/oauth-settings"));

             }

            SettingsModel::set([
                "username" => $response['user']['username'],
                "full_name" => $response['user']['full_name'],
                "user_id" => $response['user']['id'],
                "profile_picture" => $response['user']['profile_picture'],
                "access_token" => $response['access_token']
            ]);
            return redirect()->to(Backend::url("system/settings/update/techrino/instagram/oauth-settings"));




        } else if(Input::has("error")){
            if(Input::has("error_reason")){
                Flash::error("Reason: " . Input::get("error_reason") . "<br/>" . "Description" . Input::get("error_description") );
            }
            Flash::success("Logged In Successfully.");

            return redirect()->to(Backend::url("system/settings/update/techrino/instagram/oauth-settings"));
        }

    }
}
