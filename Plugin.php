<?php namespace Techrino\Instagram;

use Backend;
use System\Classes\PluginBase;

/**
 * Instagram Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Instagram',
            'description' => 'An instagram integration',
            'author'      => 'Techrino',
            'icon'        => 'icon-instagram'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
//        return []; // Remove this line to activate

        return [
            'Techrino\Instagram\Components\InstagramGallery' => 'InstagramGallery',
            'Techrino\Instagram\Components\InstagramUser' => "InstagramUser"
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {

        return [
            'techrino.instagram.access_settings' => [
                'tab' => 'Instagram',
                'label' => 'Able to modify Instagram API Authentication Settings'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [];
//        return [
//            'instagram' => [
//                'label'       => 'Instagram',
//                'url'         => Backend::url('techrino/instagram/settings'),
//                'icon'        => 'icon-instagram',
//                'permissions' => ['techrino.instagram.*'],
//                'order'       => 500,
//            ],
//        ];
    }

    public function registerSettings()
    {
        return [
            'oauth-settings' => [
                'label'       => 'Instagram API ',
                'description' => 'Settings for linking to Instagram API',
                'category'    => 'Custom Plugin',
                'icon'        => 'icon-instagram',
                'class' => 'Techrino\Instagram\Models\Settings',
                'order'       => 500,
                'keywords'    => 'instagram'
            ]
        ];
    }
}
